﻿Imports System.Runtime.InteropServices
Imports System.Drawing

Module IconExtract
    <DllImport("shell32.dll", CharSet:=CharSet.Auto)> _
    Function ExtractIconEx(ByVal szFileName As String, _
        ByVal nIconIndex As Integer, _
        ByVal phiconLarge() As IntPtr, _
        ByVal phiconSmall() As IntPtr, _
        ByVal nIcons As Integer) As Integer
    End Function
    <DllImport("user32.dll", EntryPoint:="DestroyIcon", SetLastError:=True)> _
    Function DestroyIcon(ByVal hIcon As IntPtr) As Integer
    End Function

    Public Function GetICONHandle(ByVal tsjCorePath As String, ByVal large As Boolean, ByVal lnResourceId As Integer) As IntPtr

        Dim readIconCount As Integer = 0
        Dim hDummy As IntPtr() = New IntPtr(0) {IntPtr.Zero}
        Dim hIconEx As IntPtr() = New IntPtr(0) {IntPtr.Zero}

        Try
            If (large) Then
                readIconCount = ExtractIconEx(tsjCorePath, lnResourceId, hIconEx, hDummy, 1)
            Else
                readIconCount = ExtractIconEx(tsjCorePath, lnResourceId, hDummy, hIconEx, 1)
            End If

            If (readIconCount > 0 AndAlso Not hIconEx(0).Equals(IntPtr.Zero)) Then
                Return hIconEx(0)
            Else ' NO ICONS READ
                Return Nothing
            End If
        Catch ex As Exception
            ' EXTRACT ICON ERROR
            ' BUBBLE UP
            Return Nothing
            'Throw New ApplicationException("Could not extract icon", ex)
        Finally
            'RELEASE RESOURCES
            For Each ptr As IntPtr In hIconEx
                If (Not ptr.Equals(IntPtr.Zero)) Then
                    DestroyIcon(ptr)
                End If
            Next ptr

            For Each ptr As IntPtr In hDummy
                If Not (ptr.Equals(IntPtr.Zero)) Then
                    DestroyIcon(ptr)
                End If
            Next ptr
        End Try
    End Function


    Public Function ExtractIconFromTSJCORE(ByVal tsjCorePath As String, ByVal large As Boolean, ByVal lnResourceId As Integer) As Icon

        Dim readIconCount As Integer = 0
        Dim hDummy As IntPtr() = New IntPtr(0) {IntPtr.Zero}
        Dim hIconEx As IntPtr() = New IntPtr(0) {IntPtr.Zero}

        Try
            If (large) Then
                readIconCount = ExtractIconEx(tsjCorePath, lnResourceId, hIconEx, hDummy, 1)
            Else
                readIconCount = ExtractIconEx(tsjCorePath, lnResourceId, hDummy, hIconEx, 1)
            End If

            If (readIconCount > 0 AndAlso Not hIconEx(0).Equals(IntPtr.Zero)) Then
                ' GET FIRST EXTRACTED ICON
                Dim extractedIcon As Icon = Icon.FromHandle(hIconEx(0)).Clone()
                Return extractedIcon
            Else ' NO ICONS READ
                Return Nothing
            End If
        Catch ex As Exception
            ' EXTRACT ICON ERROR
            ' BUBBLE UP
            Throw New ApplicationException("Could not extract icon", ex)
        Finally
            'RELEASE RESOURCES
            For Each ptr As IntPtr In hIconEx
                If (Not ptr.Equals(IntPtr.Zero)) Then
                    DestroyIcon(ptr)
                End If
            Next ptr

            For Each ptr As IntPtr In hDummy
                If Not (ptr.Equals(IntPtr.Zero)) Then
                    DestroyIcon(ptr)
                End If
            Next ptr
        End Try
    End Function
End Module

