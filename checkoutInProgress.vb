﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms


Public NotInheritable Class checkoutInProgress
    Inherits System.Windows.Forms.Form

    Public myRecord As TRIMSDK.Record
    Public myRecordURI As Integer
    Public myDB As TRIMSDK.Database
    Public lbDontCheckOut As Boolean
    Public wasLockedByApp As Boolean = False
    Public myProcessName As String = ""
    Public myProcessID As Integer = 0
    Public mbForcedCheckIN As Boolean ' dont want the terminiation code to fire if we forced the checkin.
    Public reminderTimer As Date = Now
    Public pathToCheckedOutFile As String = ""
    Dim myCheckoutProcess As System.Diagnostics.Process = Nothing

    Private contextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents menuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents notifyIcon1 As System.Windows.Forms.NotifyIcon
    Private mycomponents As System.ComponentModel.IContainer

    Private Sub checkoutInProgress_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        wasLockedByApp = True
        Dim nagCount As Integer = 0
        Try
            myDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, myRecordURI)
            myRecord = myDB.GetRecord(myRecord.Uri)
            If myRecord IsNot Nothing Then
                If myRecord.IsCheckedOut Then
                    If mbForcedCheckIN = False Then
                        Do While forceCheckin() <> True
                            Application.DoEvents()
                            Threading.Thread.Sleep(5000)
                            Exit Do
                            '  nagCount = nagCount + 1
                            ' MsgBox("A CM document is still checked out. Look at your Documents Checked Out folder and use Advanced to check these in.", MsgBoxStyle.Critical)
                            ' If nagCount > 2 Then Exit Do
                        Loop
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToLower.Contains("not set to an instance") Then
                End
            Else
                ' MsgBox("CM documents may still be checked out. They may not have been automatically checked back in." & vbCrLf & "Look at your Documents Checked Out folder and use Advanced to check these in." & vbCrLf & ex.Message, MsgBoxStyle.Critical)
            End If

        End Try

    End Sub

    Private Sub checkoutInProgress_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.TopMost = True
        Try

            If Not lbDontCheckOut Then

                Dim myOffLineRecord As TRIMSDK.OfflineRecord

                myOffLineRecord = myDB.NewOfflineRecordCheckout(myRecord, True)

                pathToCheckedOutFile = myOffLineRecord.FullFileName
                myRecord.SetAutoCheckin("")


                Try
                    System.IO.File.SetLastWriteTime(myOffLineRecord.FullFileName, DateTime.Now)
                Catch ex As Exception
                End Try

                myCheckoutProcess = System.Diagnostics.Process.Start(myOffLineRecord.FullFileName)
                myProcessID = myCheckoutProcess.Id
                myProcessName = myCheckoutProcess.ProcessName

            End If

            If myProcessName <> "" Then
                Me.Text = myRecord.Number & " is Checked Out:" & myProcessName & " " & myProcessID.ToString
                ' MsgBox(Me.Text)
            Else
                Me.Text = "The Document with number '" & myRecord.Number & "' has been Checked Out to you"
                wasLockedByApp = True
            End If

            Me.lblTitle.Text = "Title: " & Microsoft.VisualBasic.Left(myRecord.TypedTitle & "                                            ", 50) & " ..."
            Timer1.Enabled = True

            ' create a notify icon for this process
            Me.components = New System.ComponentModel.Container
            Me.contextMenu1 = New System.Windows.Forms.ContextMenu
            Me.menuItem1 = New System.Windows.Forms.MenuItem
            ' Initialize contextMenu1
            Me.contextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItem1})
            ' Initialize menuItem1
            Me.menuItem1.Index = 0
            Me.menuItem1.Text = "Force Checkin to CM"

            ' Create the NotifyIcon.
            Me.notifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)

            ' The Icon property sets the icon that will appear
            ' in the systray for this application.
            notifyIcon1.Icon = Me.Icon

            ' The ContextMenu property sets the menu that will
            ' appear when the systray icon is right clicked.
            notifyIcon1.ContextMenu = Me.contextMenu1

            ' The Text property sets the text that will be displayed,
            ' in a tooltip, when the mouse hovers over the systray icon.
            notifyIcon1.Text = Me.Text
            notifyIcon1.Visible = True


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Launcher")
            Me.Hide()
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim lnRet As Boolean = attemptcheckin()
    End Sub

    Private Function attemptcheckin() As Boolean

        ' every now and then we check the record to see if its still checked out.
        ' if its not then we terminate the HPRMPlus.exe process
        Application.DoEvents()
        Try
            myDB.Connect()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Launcher Auto Return Message 1")
            Return False
        End Try

        Dim tmpRecord As TRIMSDK.Record
        Try
            myDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, myRecordURI)
            tmpRecord = myDB.GetRecord(myRecord.Uri)
            ' if the record is no longer Checked Out then terminate the process
            If Not tmpRecord.IsCheckedOut Then
                tmpRecord = Nothing
                notifyIcon1.Icon = Nothing
                notifyIcon1.Visible = False
                Me.Hide()
                Return True
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Launcher Auto Return Message 2")
            Return False
        End Try

        Dim lsPath As String = ""

        ' if the file is still checkedout but no longer locked by the application then checkin and delete
        ' MsgBox("Was Locked by App status:" & wasLockedByApp.ToString & " pid:" & myProcessID)
        Try
            If wasLockedByApp Then
                ' MsgBox("Was Locked by App")
                Try
                    myDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, myRecordURI)
                    myRecord = myDB.GetRecord(myRecord.Uri)
                    If myRecord.IsCheckedOut Then
                        lsPath = myRecord.CheckedOutPath
                        If Not System.IO.File.Exists(lsPath) Then
                            lsPath = pathToCheckedOutFile
                        End If
                        ' MsgBox(lsPath)
                    Else
                        End
                    End If
                Catch ex As Exception
                    End
                End Try

                'MsgBox("path=" & lsPath)

                If myRecord.IsCheckedOut = True AndAlso System.IO.File.Exists(lsPath) Then
                    ' MsgBox("Check file in use")
                    ' MsgBox(FileInUse(lsPath).ToString)
                    If FileInUse(lsPath) = False Then
                        System.Threading.Thread.Sleep(10000) ' wait a while to see if CM takes care of the checkin for us now its no longer in use.
                        Application.DoEvents()
                        If System.IO.File.Exists(lsPath) Then
                            '    MsgBox("Check file in checked")
                            Try
                                ' touch the file to make it dirty
                                'Try
                                '    System.IO.File.AppendText(lsPath)
                                '    MsgBox("touched file")
                                'Catch
                                'End Try

                                Dim myInpDoc As New TRIMSDK.InputDocument
                                myInpDoc.SetAsFile(lsPath)
                                'If myRecord.SetDocumentUI(Me.Handle.ToInt32, myRecord.CheckedOutPath, "Return", False) Then
                                If myRecord.SetDocument(myInpDoc, True, False, "") Then
                                    myRecord.Save()
                                    ' also delete the file from the local disk on OK save
                                    Try
                                        System.IO.File.Delete(lsPath)
                                    Catch ex As Exception
                                        ' MsgBox(ex.Message & " " & lsPath, MsgBoxStyle.Critical, "Forced Checkin File Cleanup Status - EFT")
                                    End Try
                                    myDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, myRecordURI)
                                End If
                                myRecord = Nothing
                                notifyIcon1.Icon = Nothing
                                notifyIcon1.Visible = False
                                Me.Hide()
                                Return True
                            Catch
                            End Try
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Launcher Auto Return Message 3")
            Return False
        End Try


        ' OK now this file was not locked but did belong to a process. Check to see if the process still exists
        Try
            If wasLockedByApp = False And myProcessID > 0 Then
                'MsgBox("Was not Locked by App")
                Dim lnMyProcessCheck As Integer
                Try
                    lnMyProcessCheck = System.Diagnostics.Process.GetProcessById(myProcessID).Id
                Catch ex As Exception
                    lnMyProcessCheck = 0
                End Try

                If lnMyProcessCheck = 0 Then ' the process has ended so we can checkin now. works for things like Notepad
                    Dim recURI As Integer = myRecord.Uri
                    myDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, recURI)
                    myRecord = myDB.GetRecord(recURI)
                    Try
                        If myRecord.IsCheckedOut Then
                            lsPath = pathToCheckedOutFile
                            'If Not System.IO.File.Exists(lsPath) Then
                            '    lsPath = myRecord.CheckedOutPath
                            'End If
                        Else
                            '  MsgBox("File is no longer checked out")
                            End
                        End If
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                        End
                    End Try

                    Dim myInpDoc As New TRIMSDK.InputDocument
                    'If myRecord.IsCheckedOut = True AndAlso System.IO.File.Exists(lsPath) AndAlso FileInUse(lsPath) = False Then
                    '    System.Threading.Thread.Sleep(10000) ' wait a while to see if CM takes care of the checkin for us now its no longer in use.
                    '    Try
                    '        If System.IO.File.Exists(lsPath) Then
                    '            myInpDoc.SetAsFile(lsPath)
                    '            'If myRecord.SetDocumentUI(Me.Handle.ToInt32, myRecord.CheckedOutPath, "Return", False) Then
                    '            If myRecord.SetDocument(myInpDoc, True, False, "") Then
                    '                myRecord.Save()
                    '                ' also delete the file from the local disk on OK save
                    '                Try
                    '                    System.IO.File.Delete(lsPath)
                    '                Catch ex As Exception
                    '                    ' MsgBox(ex.Message & " " & lsPath, MsgBoxStyle.Critical, "Forced Checkin File Cleanup Status - EFT")
                    '                End Try
                    '                myDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, myRecordURI)
                    '            End If
                    '            myRecord = Nothing
                    '            notifyIcon1.Icon = Nothing
                    '            notifyIcon1.Visible = False
                    '            Me.Hide()
                    '        End If
                    '    Catch
                    '    End Try
                    'Else
                    '    ' file is gone so undo the checkout
                    '    myRecord.UndoCheckout()
                    'End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Launcher Auto Return Message 4")
            Return False
        End Try
        Return False
    End Function

    

    ' is the file in use , if so tell the checkout monitor program
    Private Function FileInUse(ByVal sFile As String) As Boolean
        If System.IO.File.Exists(sFile) Then
            Try
                Dim F As Short = FreeFile()
                FileOpen(F, sFile, OpenMode.Binary, OpenAccess.ReadWrite, OpenShare.LockReadWrite)
                FileClose(F)
            Catch
                Return True
            End Try
        End If
    End Function

    Private Function forceCheckin() As Boolean
        ' attempt to force a checkin
        ' MsgBox(myDB.IsConnected.ToString)
        'MsgBox(myDB.CurrentUser.FormattedName)
        'MsgBox(myRecordURI.ToString)
        Dim objRecord As TRIMSDK.Record

        Try
            myDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, myRecordURI)
            objRecord = myDB.GetRecord(myRecord.Uri)


            If objRecord.IsElectronic = True Then
                If InStr(LCase(objRecord.CheckedOutPath), "offline records") > 0 Then
                    If objRecord.SetDocumentUI(Me.Handle.ToInt32, "%topdrawer%", "Return", False) Then
                        objRecord.Save()
                        myDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, myRecordURI)
                    End If
                Else
                    Dim lsPath As String = objRecord.CheckedOutPath.ToString
                    If objRecord.SetDocumentUI(Me.Handle.ToInt32, objRecord.CheckedOutPath, "Return", False) Then
                        objRecord.Save()
                        ' also delete the file from the local disk on OK save
                        Try
                            System.IO.File.Delete(lsPath)
                        Catch ex As Exception
                            MsgBox(ex.Message & " " & lsPath, MsgBoxStyle.Critical, "Forced Checkin File Cleanup Status - Launcher C")
                        End Try
                        myDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, myRecordURI)
                    End If
                End If

            End If

            ' if the record is no longer Checked Out then terminate the process
            If Not objRecord.IsCheckedOut Then
                objRecord = Nothing
                notifyIcon1.Icon = Nothing
                notifyIcon1.Visible = False
                Me.Hide()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            ' MsgBox("Forced checked was not successful A:" & ex.Message, MsgBoxStyle.Critical, "Launcher Forced Checkin")
        End Try

    End Function

    Private Sub menuItem1_Click(ByVal Sender As Object, ByVal e As EventArgs) Handles menuItem1.Click
        Dim myRet As Boolean
        mbForcedCheckIN = True
        myRet = forceCheckin()
        If myRet = False Then
            MsgBox("Forced checked was not successful B", MsgBoxStyle.Critical, "Launcher Forced Checkin")
        End If
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.TopMost = False
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.TopMost = False
        If MsgBox("Warning, closing this window while the document is still Checked Out will stop its automated return to CM. Continue? ", MsgBoxStyle.YesNo, "Launcher") = vbYes Then
            End
        End If
    End Sub

    Private Sub Timer2_Tick(sender As System.Object, e As System.EventArgs) Handles Timer2.Tick
        Me.WindowState = FormWindowState.Minimized
    End Sub
End Class
