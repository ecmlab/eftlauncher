<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlTR5 = New System.Windows.Forms.Panel
        Me.butShowInTrimDesktop = New System.Windows.Forms.Button
        Me.btnProperties = New System.Windows.Forms.Button
        Me.btnDetails = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnView = New System.Windows.Forms.Button
        Me.pnlNative = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.btnSaveandMakeReference = New System.Windows.Forms.Button
        Me.btnSaveandDelete = New System.Windows.Forms.Button
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.btnQuit = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblrecordTitle = New System.Windows.Forms.Label
        Me.lblRecordNumber = New System.Windows.Forms.Label
        Me.lblFilePath = New System.Windows.Forms.Label
        Me.chklistFiles = New System.Windows.Forms.CheckedListBox
        Me.pnlTR5.SuspendLayout()
        Me.pnlNative.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTR5
        '
        Me.pnlTR5.Controls.Add(Me.butShowInTrimDesktop)
        Me.pnlTR5.Controls.Add(Me.btnProperties)
        Me.pnlTR5.Controls.Add(Me.btnDetails)
        Me.pnlTR5.Controls.Add(Me.btnEdit)
        Me.pnlTR5.Controls.Add(Me.btnView)
        Me.pnlTR5.Location = New System.Drawing.Point(12, 208)
        Me.pnlTR5.Name = "pnlTR5"
        Me.pnlTR5.Size = New System.Drawing.Size(342, 34)
        Me.pnlTR5.TabIndex = 4
        '
        'butShowInTrimDesktop
        '
        Me.butShowInTrimDesktop.Location = New System.Drawing.Point(4, 119)
        Me.butShowInTrimDesktop.Name = "butShowInTrimDesktop"
        Me.butShowInTrimDesktop.Size = New System.Drawing.Size(145, 23)
        Me.butShowInTrimDesktop.TabIndex = 8
        Me.butShowInTrimDesktop.Text = "Show in TRIM Desktop"
        Me.butShowInTrimDesktop.UseVisualStyleBackColor = True
        '
        'btnProperties
        '
        Me.btnProperties.Location = New System.Drawing.Point(246, 3)
        Me.btnProperties.Name = "btnProperties"
        Me.btnProperties.Size = New System.Drawing.Size(75, 23)
        Me.btnProperties.TabIndex = 7
        Me.btnProperties.Text = "Properties"
        Me.btnProperties.UseVisualStyleBackColor = True
        '
        'btnDetails
        '
        Me.btnDetails.Location = New System.Drawing.Point(165, 3)
        Me.btnDetails.Name = "btnDetails"
        Me.btnDetails.Size = New System.Drawing.Size(75, 23)
        Me.btnDetails.TabIndex = 6
        Me.btnDetails.Text = "Details"
        Me.btnDetails.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(84, 3)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnView
        '
        Me.btnView.Location = New System.Drawing.Point(3, 3)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(75, 23)
        Me.btnView.TabIndex = 4
        Me.btnView.Text = "View"
        Me.btnView.UseVisualStyleBackColor = True
        '
        'pnlNative
        '
        Me.pnlNative.Controls.Add(Me.Button1)
        Me.pnlNative.Controls.Add(Me.Button2)
        Me.pnlNative.Controls.Add(Me.btnSaveandMakeReference)
        Me.pnlNative.Controls.Add(Me.btnSaveandDelete)
        Me.pnlNative.Location = New System.Drawing.Point(12, 209)
        Me.pnlNative.Name = "pnlNative"
        Me.pnlNative.Size = New System.Drawing.Size(608, 33)
        Me.pnlNative.TabIndex = 8
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(459, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(146, 23)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Save Only"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(307, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(146, 23)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Save - Remove and Make Reference"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btnSaveandMakeReference
        '
        Me.btnSaveandMakeReference.Location = New System.Drawing.Point(155, 3)
        Me.btnSaveandMakeReference.Name = "btnSaveandMakeReference"
        Me.btnSaveandMakeReference.Size = New System.Drawing.Size(146, 23)
        Me.btnSaveandMakeReference.TabIndex = 5
        Me.btnSaveandMakeReference.Text = "Save and Make Reference"
        Me.btnSaveandMakeReference.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaveandMakeReference.UseVisualStyleBackColor = True
        '
        'btnSaveandDelete
        '
        Me.btnSaveandDelete.Location = New System.Drawing.Point(3, 3)
        Me.btnSaveandDelete.Name = "btnSaveandDelete"
        Me.btnSaveandDelete.Size = New System.Drawing.Size(146, 23)
        Me.btnSaveandDelete.TabIndex = 4
        Me.btnSaveandDelete.Text = "Save and Remove"
        Me.btnSaveandDelete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaveandDelete.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnQuit)
        Me.Panel3.Location = New System.Drawing.Point(643, 209)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(85, 32)
        Me.Panel3.TabIndex = 9
        '
        'btnQuit
        '
        Me.btnQuit.Location = New System.Drawing.Point(3, 3)
        Me.btnQuit.Name = "btnQuit"
        Me.btnQuit.Size = New System.Drawing.Size(75, 23)
        Me.btnQuit.TabIndex = 5
        Me.btnQuit.Text = "Quit"
        Me.btnQuit.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Record Title"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Record Number"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "File Path"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.Panel1.Controls.Add(Me.lblrecordTitle)
        Me.Panel1.Controls.Add(Me.lblRecordNumber)
        Me.Panel1.Controls.Add(Me.lblFilePath)
        Me.Panel1.Location = New System.Drawing.Point(97, 9)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(624, 82)
        Me.Panel1.TabIndex = 17
        '
        'lblrecordTitle
        '
        Me.lblrecordTitle.AutoSize = True
        Me.lblrecordTitle.Location = New System.Drawing.Point(3, 59)
        Me.lblrecordTitle.Name = "lblrecordTitle"
        Me.lblrecordTitle.Size = New System.Drawing.Size(65, 13)
        Me.lblrecordTitle.TabIndex = 16
        Me.lblrecordTitle.Text = "Record Title"
        '
        'lblRecordNumber
        '
        Me.lblRecordNumber.AutoSize = True
        Me.lblRecordNumber.Location = New System.Drawing.Point(3, 35)
        Me.lblRecordNumber.Name = "lblRecordNumber"
        Me.lblRecordNumber.Size = New System.Drawing.Size(82, 13)
        Me.lblRecordNumber.TabIndex = 15
        Me.lblRecordNumber.Text = "Record Number"
        '
        'lblFilePath
        '
        Me.lblFilePath.AutoSize = True
        Me.lblFilePath.Location = New System.Drawing.Point(3, 11)
        Me.lblFilePath.Name = "lblFilePath"
        Me.lblFilePath.Size = New System.Drawing.Size(48, 13)
        Me.lblFilePath.TabIndex = 14
        Me.lblFilePath.Text = "File Path"
        '
        'chklistFiles
        '
        Me.chklistFiles.CheckOnClick = True
        Me.chklistFiles.FormattingEnabled = True
        Me.chklistFiles.Location = New System.Drawing.Point(12, 97)
        Me.chklistFiles.Name = "chklistFiles"
        Me.chklistFiles.Size = New System.Drawing.Size(709, 109)
        Me.chklistFiles.Sorted = True
        Me.chklistFiles.TabIndex = 18
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(728, 244)
        Me.Controls.Add(Me.chklistFiles)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.pnlNative)
        Me.Controls.Add(Me.pnlTR5)
        Me.Name = "Main"
        Me.Text = "TRIMPlus"
        Me.pnlTR5.ResumeLayout(False)
        Me.pnlNative.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnlTR5 As System.Windows.Forms.Panel
    Friend WithEvents btnProperties As System.Windows.Forms.Button
    Friend WithEvents btnDetails As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnView As System.Windows.Forms.Button
    Friend WithEvents pnlNative As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents btnSaveandMakeReference As System.Windows.Forms.Button
    Friend WithEvents btnSaveandDelete As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnQuit As System.Windows.Forms.Button
    Friend WithEvents butShowInTrimDesktop As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblrecordTitle As System.Windows.Forms.Label
    Friend WithEvents lblRecordNumber As System.Windows.Forms.Label
    Friend WithEvents lblFilePath As System.Windows.Forms.Label
    Friend WithEvents chklistFiles As System.Windows.Forms.CheckedListBox

End Class
