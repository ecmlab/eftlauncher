﻿

Friend Class WindowsEnumerations


    Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As IntPtr, ByVal lpString As String, ByVal cch As Long) As Long

    Declare Function FindWindowEx Lib "user32.dll" Alias "FindWindowExA" (ByVal hwndParent As IntPtr, ByVal hwndChildAfter As IntPtr, ByVal lpszClass As String, ByVal lpszWindow As String) As IntPtr

    Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As IntPtr, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

    Private Const WM_CLOSE = &H10
    Private Const WM_KEYDOWN As Long = &H100


    Public Const SWP_NOMOVE = &H2
    Public Const SWP_NOSIZE = &H1
    Public Const HWND_TOPMOST = -1
    Public Const HWND_NOTOPMOST = -2
    Public Const FLAGS = SWP_NOMOVE Or SWP_NOSIZE

    Public Const WM_GETTEXT = &HD
    Public Const GWL_HWNDPARENT = (-8)

    Public gsFilename As String
    Public gbExists As Boolean

    Public Shared Function getDropDownExtType(ByVal lsProcessName As String, ByVal lsWindowName As String, ByVal lsDisplayName As String) As String
        ' scan all the process windows looking for a saveas and then get the extension type.
        ' cross check its displayname against the one we have to be sure its the same window
        ' look for top windows that match either "Properties " or "New Record "
        Dim ln_Hwnd As IntPtr
        Dim lsWindowText As String
        Dim lsContainerText As String
        Dim lsParentText As String = ""
        Dim lsGivenExtension As String = ""
        Dim lbDisplayNameMatch As Boolean = False
        lsContainerText = ""

        ln_Hwnd = FindWindowEx(0, 0, Nothing, Nothing)
        Do

            lsWindowText = Space(250)
            GetWindowText(ln_Hwnd, lsWindowText, 255)
            If lsWindowText.ToLower.Contains(lsWindowName) Then
                ' get all child windows for this top window
                Dim myAPIWindow As New WindowsEnumerator
                Dim myWindowList As List(Of ApiWindow)
                myWindowList = myAPIWindow.GetChildWindows(ln_Hwnd, "")
                Dim lnX As Integer
                For lnX = 0 To myWindowList.Count - 1
                    ' look for the matching combo box that holds the file extension details
                    If myWindowList.Item(lnX).MainWindowTitle.ToLower.Contains("(*.") And myWindowList.Item(lnX).ClassName.ToLower = "combobox" Then
                        lsGivenExtension = myWindowList.Item(lnX).MainWindowTitle
                    End If
                    ' also look for a matching text box that fits with the display name
                    If myWindowList.Item(lnX).MainWindowTitle.ToLower = lsDisplayName.Trim.ToLower Then
                        lbDisplayNameMatch = True
                    End If
                Next
            End If

            If lbDisplayNameMatch Then Return lsGivenExtension

            ln_Hwnd = FindWindowEx(0, ln_Hwnd, Nothing, Nothing)

            If ln_Hwnd = 0 Then Exit Do
        Loop
        Return ""
    End Function
    Public Shared Function isWindowStillOpen(ByVal lsWindowName As String) As Boolean
        ' see if a sub window of the parent is still open
        Dim ln_Hwnd As IntPtr
        Dim lsWindowText As String
        Dim lsParentText As String = ""
        Dim lsTestString As String = ""

        ln_Hwnd = FindWindowEx(0, 0, Nothing, Nothing)
        Do
            lsWindowText = Space(250)
            GetWindowText(ln_Hwnd, lsWindowText, 255)

            lsTestString = Replace(lsWindowText, Chr(0), "").Trim.ToLower
            If lsTestString = lsWindowName.ToLower.Trim Then
                Return True
            End If
            ln_Hwnd = FindWindowEx(0, ln_Hwnd, Nothing, Nothing)
            If ln_Hwnd = 0 Then Exit Do
        Loop
        Return False
    End Function

    Public Shared Function getTRIMSearchScreenText(ByVal lsProcessName As String, ByVal lsWindowName As String, ByVal lsDisplayName As String) As Boolean
        ' scan all the process windows looking for a saveas and then get the extension type.
        ' cross check its displayname against the one we have to be sure its the same window
        ' look for top windows that match either "Properties " or "New Record "
        Dim ln_Hwnd As IntPtr
        Dim lsWindowText As String
        Dim lsContainerText As String
        Dim lsParentText As String = ""
        Dim lsGivenExtension As String = ""
        Dim lbDisplayNameMatch As Boolean = False
        lsContainerText = ""
        Dim mySplitSet() As String = Split(lsDisplayName, "|")

        ln_Hwnd = FindWindowEx(0, 0, Nothing, Nothing)
        Do
            lsWindowText = Space(250)
            GetWindowText(ln_Hwnd, lsWindowText, 255)
            If lsWindowText.ToLower.Contains(lsWindowName.ToLower) Then
                ' get all child windows for this top window
                Dim myAPIWindow As New WindowsEnumerator
                Dim myWindowList As List(Of ApiWindow)
                myWindowList = myAPIWindow.GetChildWindows(ln_Hwnd, "")
                Dim lnX As Integer
                For lnX = 0 To myWindowList.Count - 1
                    ' look for the matching combo box that holds the file extension details
                    Dim myitem As String
                    For Each myitem In mySplitSet
                        If myWindowList.Item(lnX).MainWindowTitle.ToLower.Contains(myitem.ToLower) Then
                            Return True
                        End If
                    Next
                Next
            End If

            ln_Hwnd = FindWindowEx(0, ln_Hwnd, Nothing, Nothing)

            If ln_Hwnd = 0 Then Exit Do
        Loop
        Return False
    End Function


    Public Shared Sub closeOpenDialogs(ByVal lsWindowName As String)
        ' see if a sub window of the parent is still open
        Dim ln_Hwnd As IntPtr
        Dim lsWindowText As String
        Dim lsParentText As String = ""
        Dim lsTestString As String = ""

        ln_Hwnd = FindWindowEx(0, 0, Nothing, Nothing)
        Do
            lsWindowText = Space(250)
            GetWindowText(ln_Hwnd, lsWindowText, 255)

            lsTestString = Replace(lsWindowText, Chr(0), "").Trim.ToLower
            If lsTestString = lsWindowName.ToLower.Trim Then
                SendMessage(ln_Hwnd, WM_CLOSE, 0, 0)
            End If
            ln_Hwnd = FindWindowEx(0, ln_Hwnd, Nothing, Nothing)
            If ln_Hwnd = 0 Then Exit Do
        Loop

    End Sub

End Class
