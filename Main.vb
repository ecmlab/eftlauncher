﻿Imports System.IO


Public Class Main

    Public Const DRAGDROPOPTION_COPYMETADATA As String = "CopyMetaData"
    Public Const DRAGDROPOPTION_SAVEANDDELETE As String = "SaveAndDelete"
    Public Const DRAGDROPOPTION_CREATETR5 As String = "CreateTR5"
    Public Const DRAGDROPOPTION_LIGHTSOUT As String = "LightsOut"
    Public Const DRAGDROPOPTION_RESEQUENCE As String = "Resequence"
    Public Shared QUERYCONTAINSACTIONSEARCH As Boolean
    Public Shared QUERYCONTAINSTRAYSEARCH As Boolean
    Private Shared aTimer As System.Timers.Timer
    Public Const DEBUGON As Boolean = False ' **************************** TURM ME OFF !!!!!!!1 **************************************************
    Public Shared trimDesktopPath As String = ""
    Private Shared myTrimDB As TRIMSDK.Database
    Private Shared pbvBackFillAuthor As Boolean = False
    Private Shared myHWNDPointer As IntPtr
    Private Shared LASTSAVEDSEARCH As String = ""

    Private Shared Function SetWindowPos(hWnd As IntPtr, hWndInsertAfter As IntPtr, x As Integer, y As Integer, cx As Integer, cy As Integer,
  uFlags As Integer) As <System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)> Boolean
    End Function

    Private Const HWND_TOPMOST As Integer = -1
    Private Const SWP_NOMOVE As Integer = &H2
    Private Const SWP_NOSIZE As Integer = &H1
    Private Const SWP_SHOW As Integer = &H40


    Public Shared Function Main() As Integer


        ' set the form to topmost ?
        Dim hWnd As IntPtr = Process.GetCurrentProcess().MainWindowHandle

        showmsgbox("got windows handles")

        myHWNDPointer = Process.GetCurrentProcess().MainWindowHandle

        ' lets make this window the topmost
        Dim myTouch As New TopMostTouch
        TopMostTouch.Show()

        ' get the command line
        Dim s As String = ""

        showmsgbox("getting command line")

        'MsgBox(s)
        s = Microsoft.VisualBasic.Command()
        's = "refinesearch|45|0"
        s = Replace(s, Chr(34) & " " & Chr(34), "|")
        s = Replace(s, Chr(34), "")
        'MsgBox(s)
        'InputBox(s, s, s)
        's = "savefiletotrim|45|1914|C:\Users\rhogg.ALPHAWEST\AppData\Local\Temp\pcgucwm2.nwdautotemp.docx||-1"

        'If DEBUGON Then s = "savelastsearch|46"
        'If DEBUGON Then s = "search|46"

        showmsgbox("got command line" & s)
        ' get the command line parameters
        Dim arrParams() As String = Split(s, "|")
        'showmsgbox("split params")
        ' check the first item to see if its a trimexplorer extensions call
        ' this will look like action dbID targetrecorduri filepath

        If arrParams(0).ToLower.EndsWith(".eft") = True Then

            showmsgbox("is eft call")

            Dim sr As StreamReader = Nothing
            Dim databaseId As String = String.Empty
            Dim recordType As String = String.Empty
            Dim recordUri As String = String.Empty

            Try


                sr = File.OpenText(arrParams(0))
                Do While sr.EndOfStream = False
                    Dim line As String = sr.ReadLine()
                    If line.StartsWith("DB=") Then
                        databaseId = line.Substring(3)
                    End If

                    If line.StartsWith("URI=") Then
                        recordUri = line.Substring(4)

                    End If
                Loop
                sr.Close()

                showmsgbox("dbid is: " & databaseId)
                showmsgbox("record uri is: " & recordUri)

                myTrimDB = New TRIMSDK.Database()
                myTrimDB.Id = databaseId
                myTrimDB.Connect()

                showmsgbox("DB connect is OK : " & myTrimDB.Name)

                ' is this already checked out. let user know and offer options before they continue.
                myTrimDB.RefreshCache(TRIMSDK.btyBaseObjectTypes.btyRecord, Convert.ToInt32(recordUri.Trim))
                Dim tmpRecord As TRIMSDK.Record = myTrimDB.GetRecord(Convert.ToInt32(recordUri.Trim))




                If tmpRecord.CheckedOutTo IsNot Nothing Then

                    ' this is checked out and its checked out to us.
                    If tmpRecord.IsCheckedOut = True And tmpRecord.CheckedOutTo.Uri.ToString = myTrimDB.CurrentUser.Uri.ToString Then

                        Dim myForm As New OKToRecover
                        myForm.ShowDialog()
                        If myForm.selectedOutcome = "dismiss" Then End

                        Dim myresult2 As MsgBoxResult = MsgBox("Please confirm you want to attempt to recover a previous checked out document. Yes/No", MsgBoxStyle.YesNo, "Launcher - Confirm document recovery attempt.")
                        If myresult2 = MsgBoxResult.No Then
                            End
                        End If
                    Else
                        MsgBox("This record is currently checked out to : " & tmpRecord.CheckedOutTo.FormattedName & ". You are not allowed to edit this record at this time.", MsgBoxStyle.Exclamation, "Launcher")
                        End
                    End If
                End If

                Try
                    EditDocument(recordUri, False, False)

                Catch ex As Exception
                    MessageBox.Show("Issue found performing edit on this record " & ex.Message, "Launch Assistant")
                    myTrimDB.Disconnect()
                    myTrimDB = Nothing
                    End
                End Try


            Catch ex As Exception
                showmsgbox("major issue : " & ex.ToString)
                System.Diagnostics.Debug.WriteLine(ex.Message)
            Finally
                sr = Nothing
            End Try
        End If

    End Function



    Public Shared Function getstringValueFor2(ByRef loList As ArrayList, ByVal lsTestString As String, ByVal lsDefault As String) As String

        Dim lsItem As String
        Dim lsReturn As String = ""

        getstringValueFor2 = lsDefault
        For Each lsItem In loList
            If lsItem.ToLower.Trim.StartsWith(lsTestString.ToLower.Trim & "=") Then
                ' If InStr(lsItem.ToLower, lsTestString.ToLower) > 0 Then
                If InStr(lsItem, "=") > 0 Then
                    lsItem = lsItem & "   "
                    lsReturn = Mid(lsItem, InStr(lsItem, "=") + 1).Trim

                    ' convert tabs back the \t
                    lsReturn = Replace(lsReturn, vbTab, "\t")
                    Return lsReturn
                End If
            End If
        Next
    End Function


    Public Shared Function getLatestFileFromDirectory(ByVal lsDirPath As String) As String
        'getEFT6TempPath() & Me.Data.DBID & "-tempfilelog"
        ' scan the directory and get the first file thats not already in our "previous" list

        Dim myDir As DirectoryInfo = New DirectoryInfo(lsDirPath)
        Dim myFiles As FileSystemInfo()
        Dim lsFoundFilePath As String = ""
        Dim lsPreviousList As String = ""

        If Not System.IO.File.Exists(Common.getEFRM8xTempPath() & myTrimDB.Id & "-tempfilelog.log") Then
            Return ""
        Else
            lsPreviousList = System.IO.File.ReadAllText(Common.getEFRM8xTempPath() & myTrimDB.Id & "-tempfilelog.log")
        End If

        myFiles = myDir.GetFileSystemInfos("*")
        Dim lnX As Integer
        Try
            For lnX = 0 To myFiles.Length - 1
                'MsgBox(myFiles(lnX).Name)
                ' dont ever select tmp files , these may be here because of MS Word or other office applications
                If myFiles(lnX).Extension.ToLower <> "tmp" Then
                    If Not myFiles(lnX).Name.Contains("~") Then
                        If Not lsPreviousList.Contains(myFiles(lnX).FullName.ToLower) Then
                            ' record this files path
                            lsFoundFilePath = myFiles(lnX).FullName
                            Exit For
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "getLatestFileFromDirectory")
        End Try

        Return lsFoundFilePath
    End Function

    Private Shared Sub scrubTempDirectory(ByVal lsDirPath As String, ByVal lsPreserveThisOne As String)
        ' look at this users checkouts, if there are non then its safe to delete all files in here created 
        ' two minutes before(Now)

        Dim myRecords As TRIMSDK.Records
        Dim mySearch As TRIMSDK.RecordSearch
        Dim myDir As DirectoryInfo = New DirectoryInfo(lsDirPath)
        Dim myFiles As FileSystemInfo()
        Dim lsFoundFilePath As String = ""
        Dim ldNowLess2 As Date = DateAdd(DateInterval.Minute, -5, Now)
        Try
            mySearch = myTrimDB.NewRecordSearch
            mySearch.AddLocationClause(myTrimDB.CurrentUser, TRIMSDK.ltSearchLocationType.ltCheckedOutTo, False)
            myRecords = mySearch.GetRecords
            If myRecords.Count = 0 Then
                myFiles = myDir.GetFileSystemInfos("*")
                Dim lnX As Integer
                For lnX = myFiles.Length - 1 To 0 Step -1
                    Try
                        If myFiles(lnX).LastWriteTime < ldNowLess2 And lsPreserveThisOne.ToLower <> myFiles(lnX).FullName.ToLower Then
                            ' if the file is not in use then delete it now
                            If Not FileInUse(myFiles(lnX).FullName) Then
                                System.IO.File.Delete(myFiles(lnX).FullName)
                            End If
                        End If
                    Catch
                    End Try
                Next
            End If
        Catch
        Finally
            myRecords = Nothing
            mySearch = Nothing
        End Try

    End Sub



    ' is the file in use , if so tell the checkout monitor program
    Private Shared Function FileInUse(ByVal sFile As String) As Boolean
        If System.IO.File.Exists(sFile) Then
            Try
                Dim F As Short = FreeFile()
                FileOpen(F, sFile, OpenMode.Binary, OpenAccess.ReadWrite, OpenShare.LockReadWrite)
                FileClose(F)
            Catch
                Return True
            End Try
        End If
    End Function



    Private Shared Sub setSummaryInfo(ByVal filepath As String, ByVal lsauthor As String, ByVal lsrecordnumber As String, ByVal lsTitle As String, ByVal lsDBID As String)
        ' before we attach the document to TRIM we will update the author with the current users details
        Exit Sub

    End Sub



    Private Shared Function GetAssociatedProgram(ByVal FileExtension As String)
        ' get this from the registry
        'Computer\HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.jpg\OpenWithList
        ' get the key values for the MRUList
        ' This will be a bunch of letters
        '  MRUList > - contains ,  ecdab  , take the first letter and then get the key for that letter.
        ' Microsoft.MSPaint_8wekyb3d8bbwe!Microsoft.MSPaint is the killer app that messes up our day in MF CM 9.2
        ' if its this then we need to do a record.editdocument(0) and not a topdraw checkout.
        ' if it contains an ! then it seems to be an App and we cant use that.
        ' 
        If FileExtension.Trim.StartsWith(".") = False Then FileExtension = "." & FileExtension.ToLower
        Dim MRUList As String = ""
        Dim launchCode As String = ""
        Dim programToLaunch As String = ""
        Try
            MRUList = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\" & FileExtension & "\OpenWithList", "MRUList", 0)
            If MRUList.Trim <> "" Then
                launchCode = Left(MRUList, 1)
            End If
            programToLaunch = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\" & FileExtension & "\OpenWithList", launchCode, 0)
            If programToLaunch.Trim <> "" Then
                Return programToLaunch
            End If
        Catch ex As Exception
        End Try

        Return ""
    End Function

    ''' <summary>
    ''' edit the document
    ''' </summary>
    Private Shared Sub EditDocument(ByVal uri As Integer, Optional ByVal lbNoAutoCheckin As Boolean = False, Optional ByVal lbDontCheckOut As Boolean = False)

        Dim objRecord As TRIMSDK.Record = Nothing

        Try
            ' either just edit and drop the connection or
            ' hold open a connection to monitor its return to TRIM
            objRecord = myTrimDB.GetRecord(uri)

            If lbNoAutoCheckin Then

                If objRecord IsNot Nothing Then

                    objRecord.EditDocument(0)

                    Application.DoEvents()
                    Dim myHolder As New checkoutInProgress
                    myHolder.myDB = myTrimDB
                    myHolder.myRecord = objRecord
                    myHolder.myRecordURI = CInt(objRecord.Uri)
                    myHolder.lbDontCheckOut = True
                    myHolder.ShowDialog()
                End If
            Else

                If objRecord IsNot Nothing Then
                    ' pass up the record to the holding form and let it deal with the termination on checkin
                    Dim myHolder As New checkoutInProgress
                    myHolder.myDB = myTrimDB
                    myHolder.myRecord = objRecord
                    myHolder.myRecordURI = CInt(objRecord.Uri)
                    myHolder.lbDontCheckOut = lbDontCheckOut
                    myHolder.ShowDialog()
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            'objRecord = Nothing
            'If myTrimDB IsNot Nothing Then
            '    If myTrimDB.IsConnected Then
            '        myTrimDB.Disconnect()
            '    End If
            '    myTrimDB = Nothing
            'End If

        End Try

    End Sub




    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getEFT6TempPath() As String
        ' get back a path to an existing directory for eft6

        If System.IO.Directory.Exists(System.IO.Path.GetTempPath & "EFRM8x") Then
            Return System.IO.Path.GetTempPath & "EFRM8x\"
        Else
            Try
                MkDir(System.IO.Path.GetTempPath & "EFRM8x")
                Return System.IO.Path.GetTempPath & "EFRM8x\"
            Catch ex As Exception
                MsgBox("Could not make a temporary path to EFRM8x")
                Return ""
            End Try
        End If
    End Function


    ''' <summary>
    ''' Show the properties dialog for the trim record
    ''' </summary>
    Private Shared Function useSavedSearch(ByVal uri As Integer, ByVal lsmode As String) As Integer

        Dim objrecordSearch As TRIMSDK.RecordSearch = Nothing
        '  MsgBox("in saved search")
        If uri < 0 Then
            Try
                If lsmode = "refine" Then
                    ' we look for this users saved last search and grab it, if not just create a new search
                    Try
                        objrecordSearch = myTrimDB.GetRecordSearch(LASTSAVEDSEARCH)
                        If objrecordSearch Is Nothing Then
                            removeDummySearch(myTrimDB)
                            ' no initial personal saved search ?, lets create one for the user and take that
                            objrecordSearch = makeDummyUserSearch(myTrimDB)
                        Else
                            ' check we can access its values and not throw an error
                            Dim a As String = objrecordSearch.Name
                        End If
                    Catch ex As Exception
                        ' no initial personal saved search ?, lets create one for the user and take that
                        ' MsgBox("useSavedSearch,refine=" & ex.Message)
                        removeDummySearch(myTrimDB)

                        objrecordSearch = makeDummyUserSearch(myTrimDB)
                    End Try
                Else
                    Try
                        removeDummySearch(myTrimDB)
                        objrecordSearch = makeDummyUserSearch(myTrimDB)
                    Catch ex As Exception
                        '  MsgBox("useSavedSearch,nonrefine=" & ex.Message)
                    End Try
                End If

            Catch ex As Exception
                ' MsgBox("useSavedSearch,afternonrefine=" & ex.Message)
            End Try

        Else
            ' we have been asked to get hold of an existing search to change it.
            objrecordSearch = myTrimDB.GetRecordSearch(uri)
            ' copy this over the top of the last saved search.
            copyThisSearchOverTheTopOfTheLASTSAVEDSearch(myTrimDB, objrecordSearch.SortString, objrecordSearch.FilterString, objrecordSearch.QueryString)
        End If

        If objrecordSearch IsNot Nothing Then
            ' create this as a SDK GUI and show that to the user as the COM version does not show the correct filters or record type filters.
            ' this is even when they are correct in the trim.exe view , the COM view is broken , RM8 Dec 2013 !, rhogg
            If showandSaveLastSearchViaSDKGUI(myTrimDB) Then

                Return Convert.ToInt32(myTrimDB.GetRecordSearch(LASTSAVEDSEARCH).Uri)

                '   Return Convert.ToInt32(objrecordSearch.Uri)
            Else
                Return 0
            End If
        Else
            Return -1
        End If


    End Function

    Private Shared Function makeDummyUserSearch(mytrimdb As TRIMSDK.Database) As TRIMSDK.RecordSearch
        ' now make a new one for this user.

        ' ensure we have a clear space to work in , remove the old search if found !
        removeDummySearch(mytrimdb)
        ' MsgBox(" saved search removed ")
        Dim objrecordSearch As TRIMSDK.RecordSearch = Nothing
        objrecordSearch = mytrimdb.NewRecordSearch
        objrecordSearch.Name = LASTSAVEDSEARCH
        objrecordSearch.Description = "The last search for user " & mytrimdb.CurrentUser.FormattedName
        objrecordSearch.Owner = mytrimdb.CurrentUser
        objrecordSearch.SetAccessControlDetails(TRIMSDK.gxGeneralAccess.gxUse, TRIMSDK.asAccessControlSettings.asPrivate, mytrimdb.CurrentUser)
        objrecordSearch.SetAccessControlDetails(TRIMSDK.gxGeneralAccess.gxUpdate, TRIMSDK.asAccessControlSettings.asPrivate, mytrimdb.CurrentUser)
        objrecordSearch.SetAccessControlDetails(TRIMSDK.gxGeneralAccess.gxModifyAccess, TRIMSDK.asAccessControlSettings.asPrivate, mytrimdb.CurrentUser)
        objrecordSearch.SetAccessControlDetails(TRIMSDK.gxGeneralAccess.gxDelete, TRIMSDK.asAccessControlSettings.asPrivate, mytrimdb.CurrentUser)
        ' now save this search for later
        objrecordSearch.Save()
        Return objrecordSearch
    End Function

    Private Shared Function copyThisSearchOverTheTopOfTheLASTSAVEDSearch(mytrimdb As TRIMSDK.Database, lsSortString As String, lsFilterString As String, lsQueryString As String) As Integer

        ' we cant delete a bad search when using the COM API , need to switch to the .NET API !
        ' we will late bind the .NET SDK as we dont want our code to be binary bitness (32/64) or version dependent.
        Dim ass As System.Reflection.Assembly = Nothing
        Dim myNETDB As New Object ' HP.HPTRIM.SDK.Database()
        ' Dim uriOfLastSearch As Integer
        '  Dim tmpNewDummy As TRIMSDK.RecordSearch = Nothing
        Try
            ass = System.Reflection.Assembly.LoadFile(mytrimdb.GetBinaryFilePath & "\HP.HPTRIM.SDK.dll")
        Catch ex As Exception
            MsgBox("Unable to load dll :" & mytrimdb.GetBinaryFilePath & "\HP.HPTRIM.SDK.dll" & vbCrLf & ex.Message)
            ' Throw New Exception("Unable to load dll HP.HPTRIM.SDK.dll " & vbCrLf & ex.Message)
        End Try

        If ass Is Nothing Then
            MsgBox("Unable to load dll :" & mytrimdb.GetBinaryFilePath & "\HP.HPTRIM.SDK.dll")
        End If

        Try
            myNETDB = ass.CreateInstance("HP.HPTRIM.SDK.Database")
            myNETDB.Id = mytrimdb.Id
            myNETDB.Connect()

            ' get rid of the last saved search
            '  removeDummySearch(mytrimdb)
            ' make a fresh dummy search
            ' tmpNewDummy = makeDummyUserSearch(mytrimdb)
            ' uriOfLastSearch = Convert.ToInt32(tmpNewDummy.Uri)
            Dim myObjectTypes As Object = ass.CreateInstance("HP.HPTRIM.SDK.BaseObjectTypes")

            ' instantiate this as a SDK object and copy over the sort , filter and query strings and resave
            Dim myLastSearch As Object = myNETDB.FindTrimObjectByName(myObjectTypes.SavedSearch, LASTSAVEDSEARCH)

            ' MsgBox("Filter(" & lsFilterString & ")  Query(" & lsQueryString & ")  Sort(" & lsSortString & ")")

            myLastSearch.FilterString = lsFilterString
            myLastSearch.QueryString = lsQueryString
            myLastSearch.SortString = lsSortString
            myLastSearch.Save()
            ' MsgBox(myLastSearch.Uri.ToString)
            Dim lnRet As Integer = Convert.ToInt32(myLastSearch.Uri.ToString)
            '  MsgBox("Filter(" & myLastSearch.FilterString & ")  Query(" & myLastSearch.QueryString & ")  Sort(" & myLastSearch.SortString & ")")

            Return lnRet

        Catch ex As Exception
            MsgBox("copyThisSearchOverTheTopOfTheLASTSAVEDSearch=" & ex.Message)
        Finally
            myNETDB.Disconnect()
            '  tmpNewDummy = Nothing
        End Try

    End Function

    Private Shared Sub removeDummySearch(mytrimdb As TRIMSDK.Database)
        ' we cant delete a bad search when using the COM API , need to switch to the .NET API !
        ' we will late bind the .NET SDK as we dont want our code to be binary bitness (32/64) or version dependent.
        Dim ass As System.Reflection.Assembly = Nothing
        Try
            ass = System.Reflection.Assembly.LoadFile(mytrimdb.GetBinaryFilePath & "\HP.HPTRIM.SDK.dll")
        Catch ex As Exception
            MsgBox("Unable to load dll :" & mytrimdb.GetBinaryFilePath & "\HP.HPTRIM.SDK.dll" & vbCrLf & ex.Message)
            ' Throw New Exception("Unable to load dll HP.HPTRIM.SDK.dll " & vbCrLf & ex.Message)
        End Try

        If ass Is Nothing Then
            MsgBox("Unable to load dll :" & mytrimdb.GetBinaryFilePath & "\HP.HPTRIM.SDK.dll")
        End If

        Try
            Dim myNETDB As New Object ' HP.HPTRIM.SDK.Database()

            myNETDB = ass.CreateInstance("HP.HPTRIM.SDK.Database")

            Dim temp As String = Environment.GetEnvironmentVariable("PATH")
            If Not temp.ToLower.Contains(mytrimdb.GetBinaryFilePath.ToLower) Then
                '  MsgBox(mytrimdb.GetBinaryFilePath)
                Environment.SetEnvironmentVariable("PATH", temp & ";" & mytrimdb.GetBinaryFilePath & "\")
            End If

            myNETDB.Id = mytrimdb.Id
            myNETDB.Connect()

            If myNETDB.isconnected Then
                ' MsgBox("Connected")
            End If


            Dim myObjectTypes As Object
            myObjectTypes = ass.CreateInstance("HP.HPTRIM.SDK.BaseObjectTypes")
            ' mySearchActual = myNETDB.FindTrimObjectByName(HP.HPTRIM.SDK.BaseObjectTypes.SavedSearch, LASTSAVEDSEARCH)
            Try
                myNETDB.FindTrimObjectByName(myObjectTypes.SavedSearch, LASTSAVEDSEARCH).delete()
            Catch ex As Exception
                ' MsgBox("Issue with delete search: " & vbCrLf & ex.Message)
            End Try


            myNETDB.Disconnect()
        Catch ex As Exception
            MsgBox("removeDummySearch=" & ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' returns the URI as an integer of the last saved search
    ''' </summary>
    ''' <param name="mytrimdb"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function showandSaveLastSearchViaSDKGUI(mytrimdb As TRIMSDK.Database) As Integer


        ' we cant delete a bad search when using the COM API , need to switch to the .NET API !
        ' we will late bind the .NET SDK as we dont want our code to be binary bitness (32/64) or version dependent.
        Dim ass As System.Reflection.Assembly = Nothing
        Try
            ass = System.Reflection.Assembly.LoadFile(mytrimdb.GetBinaryFilePath & "\HP.HPTRIM.SDK.dll")
        Catch ex As Exception
            MsgBox("Unable to load dll :" & mytrimdb.GetBinaryFilePath & "\HP.HPTRIM.SDK.dll" & vbCrLf & ex.Message)
            Return False
        End Try

        If ass Is Nothing Then
            MsgBox("Unable to load dll :" & mytrimdb.GetBinaryFilePath & "\HP.HPTRIM.SDK.dll")
            Return False
        End If

        Try
            Dim myNETDB As New Object ' HP.HPTRIM.SDK.Database()

            myNETDB = ass.CreateInstance("HP.HPTRIM.SDK.Database")
            myNETDB.Id = mytrimdb.Id
            myNETDB.Connect()

            'Dim myEditor As New HP.HPTRIM.SDK.SearchUserOptions(myNETDB)

            Dim myEditor As Object = ass.CreateInstance("HP.HPTRIM.SDK.SearchUserOptions", False, System.Reflection.BindingFlags.ExactBinding, Nothing, New Object() {myNETDB}, Nothing, Nothing)

            myEditor.RecordEditor = 2 ' HP.HPTRIM.SDK.SearchEditor.Boolean

            Dim myObjectTypes As Object
            Dim mySavedSearch As Object
            myObjectTypes = ass.CreateInstance("HP.HPTRIM.SDK.BaseObjectTypes")

            Try
                mySavedSearch = myNETDB.FindTrimObjectByName(myObjectTypes.SavedSearch, LASTSAVEDSEARCH)
                Dim myMainObjectSearch As Object = ass.CreateInstance("HP.HPTRIM.SDK.TrimMainObjectSearch", False, System.Reflection.BindingFlags.ExactBinding, Nothing, New Object() {mySavedSearch}, Nothing, Nothing)
                Dim myType As Type = ass.GetType("HP.HPTRIM.SDK.ObjectSelector")
                Dim myMethods() As System.Reflection.MethodInfo = myType.GetMethods()
                Dim trimHWD As Object = ass.CreateInstance("HP.HPTRIM.SDK.Hwnd")

                For Each myMethod As System.Reflection.MethodInfo In myMethods
                    If myMethod.Name.ToLower = "editquery" Then
                        myMethod.Invoke(New Object(), {trimHWD, myMainObjectSearch})
                        Exit For
                    End If
                Next

                mySavedSearch.QueryString = myMainObjectSearch.SearchString
                mySavedSearch.SortString = myMainObjectSearch.SortString
                mySavedSearch.FilterString = myMainObjectSearch.FilterString
                mySavedSearch.Save()

                Return True

            Catch ex As Exception
                MsgBox(ex.ToString)
                Return False
            End Try

            Return False

            myNETDB.Disconnect()
        Catch ex As Exception
            MsgBox("showandSaveLastSearchViaSDKGUI=" & ex.Message)
        End Try
        Return False
    End Function




    Private Shared Sub keepThisScreenOnTop()

        ' start a handler that will watch our screen and see if it contains the following text
        ' we will set the QUERYCONTAINSACTIONSEARCH value to TRUE if we make a hit
        Try
            'Find the HPRMPlus.exe and bring it to the top
            ' properties dbID targetrecorduri filepath
            Dim objprocesses() As System.Diagnostics.Process = Nothing
            Dim aobjProcess As System.Diagnostics.Process = Nothing

            objprocesses = Process.GetProcessesByName("TRIMPlus")
            For Each aobjProcess In objprocesses

                Dim myAPIWindow As New WindowsEnumerator
                Dim myWindowList As List(Of ApiWindow)
                myWindowList = myAPIWindow.GetChildWindows(aobjProcess.Handle, "")
                Dim lnX As Integer
                For lnX = 0 To myWindowList.Count - 1
                    MsgBox(myWindowList.Item(lnX).hWnd.ToString)
                    SetWindowPos(myWindowList.Item(lnX).hWnd, New IntPtr(HWND_TOPMOST), 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE Or SWP_SHOW)
                Next
            Next
        Catch
            ' MsgBox("Error with holding window on top")
        End Try

    End Sub


    Shared Sub showmsgbox(ByVal lsmessage As String)
        If DEBUGON Then
            MsgBox(lsmessage, MsgBoxStyle.Critical, "Debug Mode")
        End If
    End Sub

    ''' <summary>
    ''' Create a connection to the TRIM database
    ''' </summary>
    Shared Function ConnectToDB(ByVal trimDatabaseID As String) As TRIMSDK.Database
        showmsgbox("getting database object")
        Dim objTrimDB As TRIMSDK.Database = Nothing
        showmsgbox("got  database object")
        Try
            objTrimDB = New TRIMSDK.Database
            showmsgbox("made new database")

            Dim myID As String = ""
            Dim myWGS As String = ""
            If trimDatabaseID.Contains("-") Then
                myID = Left(trimDatabaseID, InStr(trimDatabaseID, "-") - 1)
                myWGS = Mid(trimDatabaseID, InStr(trimDatabaseID, "-") + 1)
                objTrimDB.Id = myID
                objTrimDB.WorkgroupServerName = myWGS
            Else
                myID = trimDatabaseID
                objTrimDB.Id = myID
            End If
            objTrimDB.AutoConnect = True
            objTrimDB.Connect()
            showmsgbox("connected to database")
            Return objTrimDB
        Catch ex As Exception
            Throw ex
        End Try

    End Function

End Class
