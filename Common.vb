﻿Public Class Common

    Private Declare Function GetTempPath Lib "kernel32" Alias _
                "GetTempPathA" (ByVal nBufferLength As Long, _
                ByVal lpBuffer As String) As Long

    Private Const MAX_PATH = 260
    Public Shared myTEMPOUTLOOKPATH As String = Common.getEFRM8xTempPath() & "EFTOutlookDragDrop"
    Public Const TRIM_NULLDATE As Date = #12/30/1899#


    ''' <summary>
    ''' structure to hold the same detail we expect to see in a collection of lookup set items.
    ''' </summary>
    ''' <remarks></remarks>
    Public Structure lookupObject
        Dim Abbreviation As String
        Dim Description As String
        Dim Name As String
        Dim Uri As Integer
    End Structure





    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getEFRM8xTempPath() As String
        ' get back a path to an existing directory for eft6

        If System.IO.Directory.Exists(System.IO.Path.GetTempPath & "EFRM8x") Then
            Return System.IO.Path.GetTempPath & "EFRM8x\"
        Else
            Try
                MkDir(System.IO.Path.GetTempPath & "EFRM8x")
                Return System.IO.Path.GetTempPath & "EFRM8x\"
            Catch ex As Exception
                MsgBox("Could not make a temporary path to EFRM8x")
                Return ""
            End Try
        End If
    End Function

    Public Shared Function replaceapostswithqmarks(ByRef myInputString As String) As String
        ' when TRIM gets back a raw string search you cant have aposts inline in the text.
        ' this routing replaces them with ? marks
        ' ie.    titleword='St Andrew's' is no good , needs to be  titleword='St Andrew?s' for the RAW method to work.
        'notesword='st andrew?s';
        'titleword='st andrew?s'
        'anyword='st andrew?s'
        'fulltext='st andrew?s'
        'keyword=9
        'clastext='ADMISSION CHARGES'
        'schtext='ADMISSION CHARGES'
        Dim lsItems() As String
        Dim lsItem As String
        Dim lsMethod As String = ""
        Dim lsArgs As String = ""
        Dim lsNewOutPut As String = ""

        lsItems = Split(myInputString, ";")

        For Each lsItem In lsItems
            If InStr(lsItem, "=") > 0 Then
                Try
                    lsMethod = Left(lsItem, InStr(lsItem, "=") - 1)
                Catch
                    lsMethod = ""
                End Try
                Try
                    lsArgs = Mid(lsItem, InStr(lsItem, "=") + 1)
                Catch
                    lsArgs = ""
                End Try

                If lsMethod.ToLower.Trim = "notesword" Or _
                    lsMethod.ToLower.Trim = "titleword" Or _
                    lsMethod.ToLower.Trim = "anyword" Or _
                    lsMethod.ToLower.Trim = "fulltext" Or _
                    lsMethod.ToLower.Trim = "keyword" Or _
                    lsMethod.ToLower.Trim = "clastext" Or _
                    lsMethod.ToLower.Trim = "schtext" Then
                    If Left(lsArgs, 1) = "'" And Right(lsArgs, 1) = "'" Then
                        lsArgs = Mid(lsArgs, 2)
                        lsArgs = Left(lsArgs, lsArgs.Length - 1)
                        lsArgs = Replace(lsArgs, "'", "?")
                        lsArgs = "'" & lsArgs & "'"
                        lsNewOutPut = lsNewOutPut & ";" & lsMethod & "=" & lsArgs
                    Else
                        lsNewOutPut = lsNewOutPut & ";" & lsItem
                    End If
                Else
                    lsNewOutPut = lsNewOutPut & ";" & lsItem
                End If
            Else
                lsNewOutPut = lsNewOutPut & ";" & lsItem
            End If
        Next

        If Left(lsNewOutPut, 1) = ";" Then lsNewOutPut = Mid(lsNewOutPut, 2)
        Return lsNewOutPut
    End Function


    Private Shared Sub makeTempPath(ByVal lsString As String)
        On Error Resume Next
        MkDir(lsString)
        On Error GoTo 0
    End Sub

End Class
